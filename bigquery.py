#Antes de hacer nada:

#1. Seleccionar o crear un proyecto en Google Cloud Platform
#2. Activar la facturación en el proyecto
#3. Activar la API de Google Cloud BigQuery
#4. Autenticarse

#import libraries
from time import sleep

import google
from google.cloud import bigquery
from google.oauth2 import service_account
from google.cloud import storage


#Conseguir las credenciales de acceso a la API y crear un cliente BigQuery
from pytz import unicode

def delete_empty_dataset(dataset):
    DATASET_ID = dataset
    dataset_ref = bigquery_client.dataset(DATASET_ID)
    bigquery_client.delete_dataset(dataset_ref)

    print('Dataset {} deleted.'.format(DATASET_ID))


def delete_notempty_dataset(dataset):

    DATASET_ID = dataset
    dataset_ref = bigquery_client.dataset(DATASET_ID)
    bigquery_client.delete_dataset(dataset_ref, delete_contents=True)

    print('Dataset {} deleted.'.format(DATASET_ID))


def createTable(dataset):
    # Método para crear tablas en BigQuery

    DATASET_ID = dataset
    TABLE_ID = 'mi_tabla1'  # Nombre de ejemplo
    dataset_ref = bigquery_client.dataset(DATASET_ID)

    table_schema = [  # Esquema de ejemplo
        bigquery.SchemaField('name', 'STRING', mode='REQUIRED'),
        bigquery.SchemaField('age', 'INTEGER', mode='REQUIRED'),
        bigquery.SchemaField('location', 'STRING', mode='REQUIRED'),
    ]

    table_ref = dataset_ref.table(TABLE_ID)
    table = bigquery.Table(table_ref, schema=table_schema)
    table = bigquery_client.create_table(table)

    if table.table_id == TABLE_ID:
        print('Table {} created.'.format(TABLE_ID))


def insertRows():
    successful = False
    rows = [
        ('Paquita Salas', 23, 'Bilbao'),
        ('Peter Parker', 16, 'New York')
    ]

    DATASET_ID = 'EJEMPLO_DATASET'
    TABLE_ID = 'mi_tabla1'

    table_ref = bigquery_client.dataset(DATASET_ID).table(TABLE_ID)
    table = bigquery_client.get_table(table_ref)

    errors = bigquery_client.insert_rows(table=table, rows=rows)

    if errors == []:
        successful = True
        print("Rows inserted")

    return successful


def selectRows():

    DATASET_ID = 'EJEMPLO_DATASET'
    TABLE_ID = 'mi_tabla1'

    # Enable catching

    job_config = bigquery.QueryJobConfig()
    job_config.use_query_cache = True

    # Ejemplo de query
    query = (
        "SELECT * FROM {}.{}.{}".format(project_id, DATASET_ID, TABLE_ID)
    )

    try:
        query_job = bigquery_client.query(query, job_config=job_config)
        rows = query_job.result()
        if query_job.state == 'DONE':
            print("Se han insertado las siguientes filas: \n")
            for row in rows:
                print(row.name + " " + str(row.age) + " " + row.location)

    except Exception as e:
        print('Exception caught when trying to fetch from BigQuery\n{}'.format(unicode(e)))


def updateRows():

    DATASET_ID = 'EJEMPLO_DATASET'
    TABLE_ID = 'mi_tabla1'

    # Enable catching

    job_config = bigquery.QueryJobConfig()
    job_config.use_query_cache = True

    try:
        update_statement = (
            "UPDATE {}.{}.{} SET age = 26  WHERE name like '%Paquita%'".format(project_id, DATASET_ID, TABLE_ID))
        query_job = bigquery_client.query(update_statement, job_config=job_config)  # API request
        query_job.result()  # Waits for statement to finish

        if query_job.state == 'DONE':
            print("Se ha modificado el registro")

    except Exception as e:
        print('Exception caught when trying to fetch from BigQuery\n{}'.format(unicode(e)))


def deleteRows():

    #Método empleado para eliminar filas de una tabla

    DATASET_ID = 'EJEMPLO_DATASET'
    TABLE_ID = 'mi_tabla1'

    # Enable catching

    job_config = bigquery.QueryJobConfig()
    job_config.use_query_cache = True

    try:
        delete_statement = (
            "DELETE FROM {}.{}.{} WHERE name LIKE ('Paqu%')".format(project_id, DATASET_ID, TABLE_ID))
        query_job = bigquery_client.query(delete_statement, job_config=job_config)  # API request
        query_job.result()  # Waits for statement to finish

        if query_job.state == 'DONE':
            print("Se ha eliminado el registro")

    except Exception as e:
        print('Exception caught when trying to fetch from BigQuery\n{}'.format(unicode(e)))


if __name__== "__main__":

    credentials = service_account.Credentials.from_service_account_file('C:\keys.json')
    project_id = 'as2020-2021'
    bigquery_client = bigquery.Client(credentials=credentials, project=project_id)

    DATASET_ID = 'RANAP_DATASET'
    dataset_ref = bigquery_client.dataset(DATASET_ID)
    dataset = bigquery.Dataset(dataset_ref)
    dataset.location = 'EU'

    print("Creating dataset...\n")
    #dataset = bigquery_client.create_dataset(dataset=dataset)
    print("Dataset created...\n")

    print("Sleeping for 10 seconds...\n")
    sleep(10)

    print("Creating table...\n")
    #createTable(DATASET_ID)

    print("Sleeping for 10 seconds...\n")
    sleep(10)

    insertRows()

    print("Sleeping for 10 seconds...")
    sleep(10)

    selectRows()

    print("Sleeping for 10 seconds...\n")
    sleep(10)

    #print("Updating rows...\n")

    #updateRows()

    #print("Deleting rows...\n")

    #deleteRows()

    #Rows that were written to a table recently via streaming (using the tabledata.insertall method) cannot be
    # modified using UPDATE, DELETE, or MERGE statements. Recent writes are typically those that occur within
    # the last 30 minutes. Note that all other rows in the table remain modifiable by using UPDATE, DELETE,
    # or MERGE statements.

    #Para poder hacer un update general, teniendo en cuenta lo descrito arriba:

    #Añadir a la sentencia UPDATE, DELETE O MERGE : RowCreateTimestamp < TIMESTAMP_SUB(RowCreateTimestamp, INTERVAL 30 MINUTE)

    print("Sleeping for 10 seconds...")
    sleep(10)

    #delete_notempty_dataset(dataset=DATASET_ID)


